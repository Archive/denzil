#include <libxml/tree.h>
#include <glib.h>
#include "bookmarks.h"
#include "denzil.h"

GtkWidget *delete;

void populate_list ( GtkListStore *, GList *);
GList * bookmarks_to_list ( void );

void bookmark_close ( GtkWidget *widget, gpointer data ) {
	denzil_close_bookmark_window(bookmark_window);
	bookmark_window = NULL;
}

void selection_changed ( GtkWidget *listview ) {
	GtkTreeSelection *select;
        select = gtk_tree_view_get_selection (GTK_TREE_VIEW(listview));
	if (select) {	
		gtk_widget_set_sensitive(delete,1);
	} else {
		gtk_widget_set_sensitive(delete,0);
	}
}

void denzil_refresh_bookmarks ( GList *list ) {
	gtk_list_store_clear(GTK_LIST_STORE(model));
	populate_list(GTK_LIST_STORE(model), list);
}

void bookmark_pick ( GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *column) {
	GtkTreeSelection *select;
	GtkTreeIter iter;
	GValue value = {0, };
	select = gtk_tree_view_get_selection (view);
	gtk_tree_selection_get_selected (select, &model, &iter);
	gtk_tree_model_get_value(model, &iter, 1, &value);
	gtk_moz_embed_load_url(GTK_MOZ_EMBED(moz),g_value_get_string(&value));
	denzil_close_bookmark_window(bookmark_window);
	bookmark_window = NULL;
}

void delete_bookmark ( GtkWidget *widget, gpointer data ) {
	GtkTreeSelection *select;
        GtkTreeIter iter;
        GValue value_name = {0, };
	GValue value_url = {0, };
        select = gtk_tree_view_get_selection (GTK_TREE_VIEW(listview));
        gtk_tree_selection_get_selected (select, &model, &iter);
	gtk_list_store_remove(GTK_LIST_STORE(model),&iter);
	bookmarks = bookmarks_to_list();
	denzil_refresh_bookmarks(bookmarks);
	denzil_save_bookmarks(bookmarks);
}

GList * bookmarks_to_list ( void ) {

	GList *list = NULL;
	GtkTreeIter iter;
	gtk_tree_model_get_iter_first(model, &iter);
	
	do {
		DenzilBookmark *new = g_new0 (DenzilBookmark, 1);
		GValue name = {0, };
		GValue url = {0, };
		gtk_tree_model_get_value(model,&iter,0,&name);
		gtk_tree_model_get_value(model,&iter,1,&url);
		new->name = g_strdup(g_value_get_string(&name));
		new->url = g_strdup(g_value_get_string(&url));
		list = g_list_append (list, new);

	} while (gtk_tree_model_iter_next(model,&iter));

	return list;
}

GList * denzil_read_bookmark ( GList *list, xmlNodePtr node) {
        DenzilBookmark  *new = g_new0(DenzilBookmark,1);
                                                                                
        node = node->children;
        while (node != NULL) {
                if (g_strcasecmp(node->name,"name") == 0) {
                        new->name = g_strdup(xmlNodeGetContent(node));
                }
                if (g_strcasecmp(node->name,"url") == 0) {
                        new->url = g_strdup(xmlNodeGetContent(node));
                }
                node=node->next;
        }
        list = g_list_append (list, new);
        return list;
}

GList * denzil_load_bookmarks ( gchar *filename ) {
                                                                                
        GList           *bookmarks = NULL;
        xmlDocPtr       doc;
        xmlNodePtr      node;

	if (g_file_test (filename, G_FILE_TEST_EXISTS) == FALSE) {
		filename = g_strdup (PACKAGE_DATA_DIR 
				     "/denzil/default-bookmarks.xml");
	}
                                                                                
        doc = xmlParseFile (filename);
                                                                                
        if (! doc || g_strcasecmp(doc->children->name,"denzil-bookmarks") != 0)
                return NULL;
                                                                                
        node = doc->children->children;
                                                                                
        while (node != NULL) {
                if (g_strcasecmp(node->name,"bookmark") != 0) {
                        node=node->next;
                } else {
                        bookmarks = denzil_read_bookmark (bookmarks, node);
                        node=node->next;
                }
        }
        xmlFreeDoc (doc);
	return bookmarks;
}

gboolean denzil_save_bookmarks ( GList *list ) {
	gchar *filename = g_strdup_printf("%s/.denzil-bookmarks.xml",
					  getenv("HOME"));

	xmlDocPtr doc = xmlNewDoc (NULL);
	xmlNodePtr root_node = xmlNewDocNode(doc,NULL,"denzil-bookmarks",NULL);
	list = g_list_first (list);
	while (list != NULL) {
		DenzilBookmark *bm = list->data;
		xmlNodePtr bm_node;
		bm_node = xmlNewChild (root_node,NULL,"bookmark",NULL);
		xmlNewChild(bm_node,NULL,"name",bm->name);
		xmlNewChild(bm_node,NULL,"url",bm->url);
		list=g_list_next(list);
	}
	root_node = xmlDocSetRootElement (doc,root_node);
	xmlSaveFormatFile(filename,doc,1);
	xmlFreeDoc(doc);
	return 1;
}

GtkWidget * denzil_open_bookmark_window ( GList *bookmarks ) {

	GtkWidget *bm_vbox;
	GtkWidget *hbox;
	GtkWidget *hsep;
	GtkWidget *scrolled;
	GtkWidget *add;
	GtkWidget *close;
	GtkTreeViewColumn *name_col;
	GtkTreeViewColumn *uri_col;
	GtkCellRenderer *renderer;
	GtkListStore *store;
	GList *list;

	if (bookmark_window != NULL) {
		bookmarks = bookmarks_to_list();
		gtk_widget_destroy(bookmark_window);
		bookmark_window = NULL;
		return NULL;
	}

	bm_vbox = gtk_vbox_new (0,0);
	hbox = gtk_hbox_new (0,1);
	hsep = gtk_hseparator_new();
	add = icon_button("gtk-add",1);
	delete = icon_button("gtk-remove",0);
	close = icon_button("gtk-close",1);
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING);
	model = GTK_TREE_MODEL(store);
	renderer = gtk_cell_renderer_text_new ();
	name_col = gtk_tree_view_column_new_with_attributes ("Name",
		renderer,"text",0,NULL);
	gtk_tree_view_column_set_sort_column_id (name_col, 0);
	uri_col = gtk_tree_view_column_new_with_attributes ("Location",
		renderer,"text",1,NULL);
	gtk_tree_view_column_set_sort_column_id (uri_col, 1);
	listview = gtk_tree_view_new_with_model (model);
	gtk_tree_view_append_column (GTK_TREE_VIEW(listview), name_col);
	gtk_tree_view_append_column (GTK_TREE_VIEW(listview), uri_col);
	gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE(model),0,
					      GTK_SORT_ASCENDING);
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW(listview),TRUE);
	gtk_container_add(GTK_CONTAINER(scrolled),GTK_WIDGET(listview));
	gtk_box_pack_start (GTK_BOX(bm_vbox),GTK_WIDGET(scrolled),1,1,1);
	gtk_box_pack_start (GTK_BOX(bm_vbox),GTK_WIDGET(hsep),0,0,1);
	gtk_box_pack_start (GTK_BOX(bm_vbox),GTK_WIDGET(hbox),0,0,1);
	gtk_box_pack_start (GTK_BOX(hbox),GTK_WIDGET(add),0,0,3);
	gtk_box_pack_start (GTK_BOX(hbox),GTK_WIDGET(delete),0,0,3);
	gtk_box_pack_start (GTK_BOX(hbox),GTK_WIDGET(close),0,0,3);
	list = g_list_first(bookmarks);
	populate_list(store, list);
	g_signal_connect(G_OBJECT(listview),"row-activated",
			 G_CALLBACK(bookmark_pick),NULL);
	g_signal_connect(G_OBJECT(listview),"cursor-changed",
			 G_CALLBACK(selection_changed),NULL);
	g_signal_connect(G_OBJECT(add),"clicked",G_CALLBACK(add_bm),moz);
	g_signal_connect(G_OBJECT(delete),"clicked",G_CALLBACK(delete_bookmark),
			 NULL);
	g_signal_connect(G_OBJECT(close),"clicked",G_CALLBACK(bookmark_close),
			 NULL);
	gtk_box_pack_start(GTK_BOX(vbox),GTK_WIDGET(bm_vbox),1,1,0);
	gtk_widget_show_all(GTK_WIDGET(bm_vbox));
	return bm_vbox;
}

void populate_list ( GtkListStore *store, GList *list ) {
	GtkTreeIter iter;
        while (list != NULL) {
                DenzilBookmark *node = list->data;
                gtk_list_store_append (store, &iter);
                gtk_list_store_set (store, &iter,0,node->name,1,node->url,-1);
                list = g_list_next(list);
        }
}

void denzil_close_bookmark_window ( GtkWidget *window ) {
	bookmarks = bookmarks_to_list();
	gtk_widget_destroy (window);
}

GList * denzil_add_bookmark_to_list ( GList *list, gchar *name, gchar *url ) {

	DenzilBookmark *new = g_new0(DenzilBookmark,1);
	new->name = g_strdup(name);
	new->url = g_strdup(url);
	list = g_list_append(list, new);
	denzil_save_bookmarks(list);
	return list;

}

