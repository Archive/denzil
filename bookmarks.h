#include <gtk/gtk.h>
#include <libxml/tree.h>

typedef struct {
	gchar *name;
	gchar *url;
} DenzilBookmark;

GtkWidget *bookmark_window;
GtkWidget *listview;
GtkTreeModel *model;
GList *bookmarks;

GList * denzil_read_bookmark ( GList *, xmlNodePtr );
GList * denzil_load_bookmarks ( gchar * );
gboolean denzil_save_bookmarks ( GList * );
GtkWidget * denzil_open_bookmark_window ( GList * );
void denzil_close_bookmark_window ( GtkWidget * );
GList * denzil_add_bookmark_to_list ( GList *, gchar *, gchar * );
