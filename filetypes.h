#include <glib.h>
#include <libxml/tree.h>

typedef enum {
	DENZIL_NOACTION,
	DENZIL_DOWNLOAD,
	DENZIL_LAUNCH,
	DENZIL_SPECIAL
} DenzilAction;

typedef struct {
	gchar		*extension;
	DenzilAction	action;
	gchar		*data;
} DenzilFileType;

GList * denzil_parse_file_types ( gchar * );
GList * denzil_add_type ( GList *, xmlNodePtr );
DenzilFileType * denzil_check_file_type ( GList *, gchar * );

