#include <glib.h>
#include <libxml/tree.h>
#include "filetypes.h"

GList * denzil_add_type ( GList *list, xmlNodePtr node) {
	DenzilFileType	*new = g_new0(DenzilFileType,1);

	node = node->children;
	while (node != NULL) {
		if (g_strcasecmp(node->name,"extension") == 0) {
			new->extension = g_strdup(xmlNodeGetContent(node));
		}
                if (g_strcasecmp(node->name,"action") == 0) {
                        new->action = atoi(xmlNodeGetContent(node));
                }
                if (g_strcasecmp(node->name,"data") == 0) {
                        new->data = g_strdup(xmlNodeGetContent(node));
                }
		node=node->next;
	}
	list = g_list_append (list, new);
	return list;
}

GList * denzil_parse_file_types ( gchar *filename ) {

	GList		*filetypes = NULL;
	xmlDocPtr	doc;
	xmlNodePtr	node;

	doc = xmlParseFile (filename);

	if (! doc || g_strcasecmp(doc->children->name,"denzil") != 0) 
		return NULL;

	node = doc->children->children;

	while (node != NULL) {
		if (g_strcasecmp(node->name,"filetype") != 0) {
			node=node->next;
		} else {
			filetypes = denzil_add_type (filetypes, node);
			node=node->next;
		}
	}
	xmlFreeDoc (doc);
	return filetypes;
};

DenzilFileType * denzil_check_file_type ( GList *list, gchar *uri ) {
	DenzilFileType *type = NULL;
	list = g_list_first(list);
	while (list != NULL) {
		if (g_str_has_suffix(uri,
				((DenzilFileType *)list->data)->extension)) {
			type = list->data;
			return type;
		}
		list=g_list_next(list);
	}
	return NULL;
}
