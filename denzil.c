#include <gtk/gtk.h>
#include <gtkmozembed.h>
#include "denzil.h"
#include "filetypes.h"
#include "bookmarks.h"

/* status flags */
#define DENZIL_START_LOAD 65552
#define DENZIL_STOP_LOAD 786448

/* ugly global mainwindow etc */
static GtkWidget *win;
static GtkWidget *entry;
static GtkWidget *back;
static GtkWidget *forward;
static GtkWidget *reload;
static GtkWidget *stop;
static GtkWidget *bookmark;
static GtkWidget *add_bookmark;
GList *filetypes;
gchar *bookmark_file;

/* callbacks for the back/fwd button click events */
void go_back (GtkWidget *widget, gpointer html) {
	gtk_moz_embed_go_back(html);
}

void go_fwd (GtkWidget *widget, gpointer html) {
	gtk_moz_embed_go_forward(html);
}

/* utility function to make a gtk button with a stock icon only */
GtkWidget *icon_button (gchar *stock_id, gboolean sensitivity) {
	GtkWidget *button;
	GtkWidget *image;

	button=gtk_button_new();
	image = gtk_image_new_from_stock(stock_id,GTK_ICON_SIZE_BUTTON);
	gtk_container_add(GTK_CONTAINER(button),GTK_WIDGET(image));
	gtk_widget_set_sensitive(GTK_WIDGET(button),sensitivity);
	return button;
}

/* callback to load URL when return keyed in entry widget */
void entry_changed (GtkWidget *widget, gpointer html) {

	const gchar *url;

	url = gtk_entry_get_text(GTK_ENTRY(widget));
	gtk_widget_set_sensitive(GTK_WIDGET(stop),1);
	gtk_widget_set_sensitive(GTK_WIDGET(reload),1);
	gtk_moz_embed_load_url(GTK_MOZ_EMBED(html),url);
}

void add_bm (GtkWidget *widget, gpointer html) {

	gchar *name = gtk_moz_embed_get_title(GTK_MOZ_EMBED(html));
	gchar *url = gtk_moz_embed_get_location(GTK_MOZ_EMBED(html));
	bookmarks = denzil_add_bookmark_to_list(bookmarks, name, url);
	if (bookmark_window != NULL) {
		denzil_refresh_bookmarks (bookmarks);
	}
}

/* detect URL loading status and update UI accordingly */
void status_changed (GtkMozEmbed *html,gint state,guint status) {
	gchar *title;
	gchar *location;

	/* we're loading a URL - set title, URL entry, and controls */
	if (state==DENZIL_START_LOAD) {
		title = gtk_moz_embed_get_title(GTK_MOZ_EMBED(html));
		location = gtk_moz_embed_get_location(GTK_MOZ_EMBED(html));
		gtk_window_set_title(GTK_WINDOW(win),title);
		gtk_entry_set_text(GTK_ENTRY(entry),location);
		gtk_widget_set_sensitive(GTK_WIDGET(stop),1);
		gtk_widget_set_sensitive(GTK_WIDGET(add_bookmark),0);
		g_free(title);
		g_free(location);
	}

	/* we're either done or cancelled, update controls */
	if (state==DENZIL_STOP_LOAD) {
		gtk_widget_set_sensitive(GTK_WIDGET(stop),0);
	 	gtk_widget_set_sensitive(GTK_WIDGET(add_bookmark),1);
	}

	gtk_widget_set_sensitive(back,gtk_moz_embed_can_go_back(html));
	gtk_widget_set_sensitive(forward,gtk_moz_embed_can_go_forward(html));
}

gint download_dialog (gchar *uri, gchar *action) {

	GtkWidget *dialog;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *icon;
	gchar *message;
	gint response;
	GError *error;

	/* construct a dialog with yes/no buttons and the
	* business end of the filename to download */ 
	message = g_strdup_printf ("Do you want to %s:\n%s",
				  action, g_path_get_basename (uri));
	icon = gtk_image_new_from_stock (GTK_STOCK_CONVERT,
					 GTK_ICON_SIZE_DIALOG);
	label = gtk_label_new (message);
	hbox = gtk_hbox_new (0,6);
	gtk_box_pack_start (GTK_BOX(hbox),GTK_WIDGET(icon),1,1,6);
	gtk_box_pack_start (GTK_BOX(hbox),GTK_WIDGET(label),0,1,6);
	g_free (message);
	dialog=gtk_dialog_new_with_buttons ("File Action", GTK_WINDOW(win),
					    GTK_DIALOG_MODAL |
					    GTK_DIALOG_DESTROY_WITH_PARENT,
					    GTK_STOCK_NO,GTK_RESPONSE_NO,
					    GTK_STOCK_YES,GTK_RESPONSE_YES,
					    NULL);

	gtk_container_add (GTK_CONTAINER (GTK_DIALOG(dialog)->vbox), hbox);
	gtk_widget_show_all (dialog);
	response = gtk_dialog_run (GTK_DIALOG(dialog));
	gtk_widget_destroy (dialog);
	return response;
}
					    

/* Is this something we should be downloading? */
gint check_download (GtkMozEmbed *html, gchar *uri) {

	/* ugliness - check manually for common downloads */
	DenzilFileType *type;
	gchar *command[1];
	gchar *action[] = {"","download","launch","do stuff with"};
	gint choice, pid;
	GError *error;

	type = denzil_check_file_type (filetypes, uri);
	if (type == NULL || type->action == 0) return 0;

	choice = download_dialog(uri, action[type->action]);

	/* if user said no to launch, how about a download? */
	if (choice != GTK_RESPONSE_YES && type->action == DENZIL_LAUNCH) {
		type->action = DENZIL_DOWNLOAD;
		choice = download_dialog (uri, action[type->action]);
	}

	/* user said no */
	if (choice != GTK_RESPONSE_YES) return 1;
		
	if (type->action == DENZIL_DOWNLOAD) {
	  command[0] = g_strdup(WGET_CMD);
	} else {
	  command[0] = g_strdup(type->data);
	}

	command[1] = g_strdup(uri);
	command[2] = NULL;

	g_spawn_async(g_get_home_dir(),command,NULL,0,NULL,NULL,&pid,&error);
	return 1;
}

void bookmark_show (GtkWidget *button, gpointer data) {

	bookmark_window = denzil_open_bookmark_window (bookmarks);
}

void reload_page (GtkWidget *button, GtkMozEmbed *html) {
	gtk_moz_embed_reload(GTK_MOZ_EMBED(html),
			     GTK_MOZ_EMBED_FLAG_RELOADNORMAL);
}

void stop_load (GtkWidget *button, GtkMozEmbed *html) {
	gtk_moz_embed_stop_load(GTK_MOZ_EMBED(html));
	gtk_widget_set_sensitive(GTK_WIDGET(stop),0);
}

int main ( int argc, char *argv[] ) {

	GtkWidget *hbox;

	bookmark_file = g_strdup_printf( "%s/.denzil-bookmarks.xml",
					getenv("HOME"));

	filetypes = denzil_parse_file_types 
				(PACKAGE_DATA_DIR "/denzil/filetypes.xml");
	bookmarks = denzil_load_bookmarks (bookmark_file);
	gtk_init(&argc,&argv);

	/* set up widgets, etc. */
	
	win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	moz = gtk_moz_embed_new();
	entry = gtk_entry_new();
        gtk_window_set_icon_from_file(GTK_WINDOW(win),
				PACKAGE_DATA_DIR "/pixmaps/denzil.png",NULL);
	vbox = gtk_vbox_new(0,0);
	hbox = gtk_hbox_new(0,0);
	back = icon_button("gtk-go-back",0);
	forward = icon_button("gtk-go-forward",0);
	reload = icon_button("gtk-refresh",0);
	stop = icon_button("gtk-stop",0);
	bookmark = icon_button("gtk-find",1);
	add_bookmark = icon_button("gtk-add",0);
	gtk_window_set_default_size(GTK_WINDOW(win),640,480);
	gtk_window_set_title(GTK_WINDOW(win),"Denzil");
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(back),0,1,3);
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(forward),0,1,3);
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(reload),0,1,3);
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(stop),0,1,3);
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(bookmark),0,1,3);
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(add_bookmark),0,1,3);
	gtk_box_pack_start(GTK_BOX(hbox),GTK_WIDGET(entry),1,1,3);
	gtk_box_pack_start(GTK_BOX(vbox),GTK_WIDGET(hbox),0,0,3);
	gtk_box_pack_start(GTK_BOX(vbox),GTK_WIDGET(moz),1,1,1);

	/* application window signals */
	g_signal_connect(G_OBJECT(entry),"activate",G_CALLBACK(entry_changed),
			 moz);
	g_signal_connect(G_OBJECT(win),"destroy",G_CALLBACK(gtk_main_quit),
			 NULL);
	g_signal_connect(G_OBJECT(win),"delete-event",
                         G_CALLBACK(gtk_main_quit),NULL);


	/* button control signals */
	g_signal_connect(G_OBJECT(back),"clicked",G_CALLBACK(go_back),moz);
	g_signal_connect(G_OBJECT(forward),"clicked",G_CALLBACK(go_fwd),moz);
	g_signal_connect(G_OBJECT(reload),"clicked",
			 G_CALLBACK(reload_page),moz);
	g_signal_connect(G_OBJECT(stop),"clicked",G_CALLBACK(stop_load),moz);
	g_signal_connect(G_OBJECT(bookmark),"clicked",
			 G_CALLBACK(bookmark_show),NULL);
	g_signal_connect(G_OBJECT(add_bookmark),"clicked",G_CALLBACK(add_bm),
			 moz);

	/* gtkmozembed widget signals */
	g_signal_connect(G_OBJECT(win),"delete-event",
			 G_CALLBACK(gtk_main_quit),NULL);
	g_signal_connect(G_OBJECT(moz),"net-state",
			 G_CALLBACK(status_changed),NULL);
	g_signal_connect(G_OBJECT(moz),"open-uri",
			 G_CALLBACK(check_download),NULL);

	/* if there was a URL on the command line, load it */

	if (argc>1) {
		gtk_moz_embed_load_url(GTK_MOZ_EMBED(moz),argv[1]);
	} else {
		gtk_moz_embed_load_url(GTK_MOZ_EMBED(moz),
			      PACKAGE_DATA_DIR "/denzil/denzil-home.html");
	}

	/* start things up */

	gtk_container_add(GTK_CONTAINER(win),GTK_WIDGET(vbox));	
	gtk_widget_show_all(win);
	gtk_main();
	g_free(bookmark_file);
	g_list_free(filetypes);
	g_list_free(bookmarks);
}
